#include "Balise.h"
#include <cstring>
#include <arpa/inet.h>

Balise::Balise(const std::string & boat_name, int xPos, int yPos, int xVel, int yVel)
{
    m_coords.x = xPos;
    m_coords.y = yPos;
    m_velocity.x = xVel;
    m_velocity.y = yVel;
    strncpy(m_name,boat_name.c_str(),LENGTH);
}

Balise::Balise()
{

}

std::string Balise::getName() const
{
    return m_name;
}

Coords Balise::getCoords() const
{
    return m_coords;
}

Coords Balise::getVelocity() const
{
    return m_velocity;
}

std::string Balise::toString() const
{
    return "NAME : " + getName() + ", " + getCoords().toString();
}

void Balise::updateCoords()
{
    m_coords.x += m_velocity.x;
    m_coords.y += m_velocity.y;
}

bool Balise::CoordsinBounds()
{
    return (m_coords.x <= 800 + m_velocity.x && m_coords.x >= -800 - m_velocity.x && m_coords.y <= 450 + m_velocity.y && m_coords.y >= -450 - m_velocity.y);
}
