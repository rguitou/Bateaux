#ifndef BALISE_H
#define BALISE_H

#include <string>
#include <arpa/inet.h>

// Classe contenant les infos des balises

struct Coords
{
    int x;
    int y;

    int getX()
    {
        return x;
    }

    int getY()
    {
        return y;
    }

    std::string toString()
    {
        return "X = " + std::to_string(getX()) + ", Y = " + std::to_string(getY());
    }
};


class Balise
{

    static const int LENGTH = 20;
    char m_name[LENGTH];
    Coords m_coords;
    Coords m_velocity;

public:
    // constructor
    Balise(const std::string & boat_name, int xPos, int yPos, int xVel, int yVel);
    Balise();

    // getters
    std::string getName() const;
    Coords getCoords() const;
    Coords getVelocity() const;
    std::string toString() const;
    void updateCoords();
    bool CoordsinBounds();
};

enum class BaliseAction
{
    New, Delete
};

using NewBalise = std::pair<BaliseAction, Balise*>;


#endif // BALISE_H
