#ifndef BUFFER_H
#define BUFFER_H

#include <vector>
#include <atomic>

// Classe permettant d'envoyer des objets T d'un thread à un autre
// sans utiliser de mutex.
// Ne fonctionne qu'avec un seul thread en lecture et un seul autre
// en écriture

#define WAIT_FOR(condition) while(!condition){}

template<typename T>

class Buffer
{
private:
    std::vector<T> m_buffer;

    unsigned int m_read_position = 0, m_write_position = 0;

    std::atomic<unsigned int> m_length;

public:

    // Constructeur du buffer
    // Par défaut, sa taille est de 100
    Buffer(unsigned int buffer_size = 100)
        :m_length{0}
    {
        m_buffer.resize(buffer_size);
    }

    // Ajoute un objet au buffer
    // Si le buffer est déjà remplit,
    // attend qu'un emplacement se libère
    void push(T & obj)
    {
        WAIT_FOR( canInsert() );

        m_buffer[m_write_position] = obj;

        m_length++;
        nextPosition(m_write_position);
    }

    // Récupère un objet du buffer
    // Si le buffer est vide,
    // attend qu'un objet soit inséré
    T& pop()
    {
        WAIT_FOR( canRead() );

        T& value = m_buffer[m_read_position];

        m_length--;
        nextPosition(m_read_position);

        return value;
    }

    // Renvoit vrai si le buffer n'est pas complétement remplit,
    // faux sinon
    // Permet d'éviter d'être bloqué par push()
    bool canInsert() const
    {
        return m_length < m_buffer.size();
    }

    // Renvoit vrai si le buffer n'est pas vide,
    // faux sinon
    // Permet d'éviter d'être bloqué par pop()
    bool canRead() const
    {
        return m_length > 0;
    }

    // Renvoit le nombre d'objets pouvant être écrit
    unsigned int availableSize() const
    {
        return m_buffer.size() - m_length;
    }

    // Renvoit les objets en attente de lecture
    unsigned int objectsInQueue() const
    {
        return m_length;
    }

private:

    // Fait passer une position à la position suivante
    void nextPosition(unsigned int & actual_position)
    {
        actual_position = ( actual_position + 1 ) % m_buffer.size();
    }
};

#endif // BUFFER_H
