#include "TcpListener.h"

TcpListener::TcpListener()
{
    m_socket = socket(AF_INET, SOCK_STREAM, 0);
}

void TcpListener::listenOn(unsigned int port, unsigned int queue_size)
{
    if(port > 1024)
    {
        m_local_ip = local_socket_address(SOCK_STREAM, std::to_string(port));
        bind(m_socket, (sockaddr*)&m_local_ip, sizeof(m_local_ip));
        listen(m_socket, queue_size);
    }
}

TcpSocket* TcpListener::acceptConnection()
{
    int newSocket;
    sockaddr_in newIp;
    socklen_t size = sizeof(newIp);

    newSocket = accept(m_socket, (sockaddr*)&newIp, &size);

    return new TcpSocket(newSocket, newIp);
}

void TcpListener::closeSocket()
{
    close(m_socket);
}
