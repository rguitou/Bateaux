#ifndef TCPLISTENER_H
#define TCPLISTENER_H
#include "TcpSocket.h"

// Classe pour accepter de nouvelles connexions TCP

class TcpListener
{
private:

    sockaddr_in m_local_ip;

    int m_socket;

public:
    TcpListener();

    // Permet au listener d'écouter un port, avec une file
    // d'attente de taile spécifiée
    void listenOn(unsigned int port, unsigned int queue_size);

    // Accepte une connexion entrante sous la forme
    // d'un TcpSocket
    TcpSocket* acceptConnection();

    void closeSocket();
};

#endif // TCPLISTENER_H
