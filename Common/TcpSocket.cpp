#include "TcpSocket.h"

TcpSocket::TcpSocket()
{
    m_socket = socket(AF_INET, SOCK_STREAM, 0);
}

TcpSocket::TcpSocket(int socket, sockaddr_in ip)
{
    m_socket = socket;
    m_remote_ip = ip;
}

bool TcpSocket::connectTo(const std::string & ip, const std::string & port)
{
    bool success = true;

    try{
         m_remote_ip = remote_socket_address(ip, SOCK_STREAM, port);
    }catch(const char * & str)
    {
        success = false;
    }

    if(connect(m_socket, (sockaddr *)&m_remote_ip, sizeof(m_remote_ip))
            == -1)
        success = false;

    isConnected = true;

    return success;
}

void TcpSocket::closeSocket()
{
    close(m_socket);
}

TcpSocket::~TcpSocket()
{
    closeSocket();
}
