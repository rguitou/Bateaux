#ifndef TCPSOCKET_H
#define TCPSOCKET_H
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <signal.h>

#include "make-sockaddr.h"

// Classe représentant une connexion TCP

class TcpSocket
{
private:

    sockaddr_in m_remote_ip;

    int m_socket;

    bool isConnected;

public:

    // Construit un TcpSocket vide
    TcpSocket();

    // Construit un TcpSocket à partir
    // d'un socket déjà existant
    TcpSocket(int socket, sockaddr_in ip);

    ~TcpSocket();

    // Connecte le TcpSocket à l'ip spécifiée,
    // au port spécifié.
    // Renvoit vrai si la connexion est bien effectuée,
    // faux sinon
    bool connectTo(const std::string & ip, const std::string & port);

    // Envoit un objet T
    // Renvoit vrai si l'objet s'est bien envoyé,
    // faux sinon
    template<typename T>
    bool sendObject(const T & object)
    {

        if(send(m_socket, &object, sizeof(object), MSG_NOSIGNAL) != sizeof(object))
        {
            isConnected = false;
            return false;
        }
        return true;
    }

    // Reçoit un objet T
    // Renvoit vrai si l'objet est bien arrivé,
    // faux sinon
    template<typename T>
    bool receiveObject(T & buffer)
    {
        return recv(m_socket, &buffer, sizeof(buffer), 0) > 0;
    }

    // Renvoit l'ip du destinataire
    sockaddr_in getIp() const { return m_remote_ip; }

    // Ferme la connexion
    void closeSocket();
};

#endif // TCPSOCKET_H
