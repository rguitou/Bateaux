#include "UdpSocket.h"

UdpSocket::UdpSocket(unsigned int port)
    :m_canReceive{true}
{
    m_local_adress = local_socket_address(SOCK_DGRAM, std::to_string(port));
    m_socket = socket(AF_INET, SOCK_DGRAM, 0);
    bind(m_socket, (sockaddr *) &m_local_adress, sizeof(m_local_adress));
}

UdpSocket::UdpSocket()
    :m_canReceive{false}
{
    m_socket = socket(AF_INET, SOCK_DGRAM, 0);
}

