#ifndef UDPSOCKET_H
#define UDPSOCKET_H

#include <unistd.h>

#include "make-sockaddr.h"

// Classe permettant d'envoyer des données en UDP

class UdpSocket
{
private:
    int m_socket;
    sockaddr_in m_local_adress;
    bool m_canReceive;

public:
    // Constructeur pour envoyer / recevoir des données
    // sur le port specifie
    UdpSocket(unsigned int port);

    // Constructeur fournissant un socket uniquement
    // capable d'envoyer des données
    UdpSocket();

    // Envoit un objet T
    // Renvoit vrai s'il s'est bien envoyé,
    // faux sinon
    template<typename T>
    bool sendObject(const T & object, std::string ip, unsigned int port)
    {
        sockaddr_in remote_ip = remote_socket_address(ip, SOCK_DGRAM, std::to_string(port));
        return sendto(m_socket, &object, sizeof(object), 0,
               (sockaddr * ) &remote_ip, sizeof(remote_ip)) != -1;
    }

    // Reçoit un objet T
    // Renvoit vrai si l'objet reçu est bien de la taille attendu,
    // faux sinon
    template<typename T>
    bool receiveObject(T & buffer)
    {
        if(!m_canReceive)
            return false;

        return read(m_socket, &buffer, sizeof(buffer))
                == sizeof(buffer);
    }

    // Reçoit un objet T venant du port spécifié
    // Renvoit vrai si l'objet reçu est bien de la taille attendu,
    // faux sinon
    template<typename T>
    bool receiveObject(T & buffer, unsigned int port)
    {
        if(!m_canReceive)
        {
            m_local_adress = local_socket_address(SOCK_DGRAM, std::to_string(port));
            bind(m_socket, (sockaddr *) &m_local_adress, sizeof(m_local_adress));
            m_canReceive = true;
        }

        return read(m_socket, &buffer, sizeof(buffer))
                == sizeof(buffer);
    }
};

#endif // UDPSOCKET_H
