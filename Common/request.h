#ifndef REQUEST_H
#define REQUEST_H

#include <string>

class Request
{
    static const int TAILLE_MAX = 20;
    char m_nom[TAILLE_MAX];  // tableau de caractères

   public:
    // constructeurs
    Request ()                                   = default;
    Request (const std::string & un_nom);

    // accesseurs
    std::string   nom()     const;
};

#endif // REQUEST_H
