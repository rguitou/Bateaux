TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra

INCLUDEPATH += -I/usr/include
LIBS += -L/usr/lib/ -lsfml-system -lsfml-graphics -lsfml-window -pthread

SOURCES += main.cpp \
    ../Common/request.cpp \
    ../Common/make-sockaddr.cc \
    ../Common/TcpSocket.cpp \
    ../Common/Balise.cpp \
    ../Common/Buffer.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../Common/request.h \
    ../Common/make-sockaddr.h \
    ../Common/TcpSocket.h \
    ../Common/Balise.h \
    ../Common/Buffer.h

