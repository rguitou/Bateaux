#include <iostream>
#include <unistd.h>
#include <string>
#include <thread>
#include <mutex>
#include <SFML/Graphics.hpp>
#include <map>

#include "../Common/make-sockaddr.h"
#include "../Common/request.h"
#include "../Common/TcpSocket.h"
#include "../Common/Balise.h"
#include "../Common/Buffer.h"

using namespace std;

#define LOG(message) cout << message << endl;

void receive(Buffer<NewBalise *> &buffer, TcpSocket & socket);

bool doesBaliseExist(const std::map<std::string, Balise*> & balises, std::string name);

int main(int argc, char* argv[])
{
    // Création de la liste de balises et de buffer
    std::map<std::string, Balise*> balises;
    Buffer<NewBalise*> balises_buffer;

    // Création de la fenêtre de rendu
    sf::RenderWindow window(sf::VideoMode(1600,900), "Consultation");

    // Création du background
    sf::RectangleShape background({1600, 900});
    sf::Texture ocean;
    ocean.loadFromFile("ocean.jpg");
    background.setTexture(&ocean);

    // Création du rendu d'un bateau
    sf::RectangleShape boat({20, 20});
    boat.setFillColor(sf::Color::Red);
    boat.setOrigin(boat.getSize().x / 2, boat.getSize().y / 2);

    // Création de la police
    sf::Font font;
    sf::Text text;
    font.loadFromFile("font.otf");
    text.setFont(font);
    text.setCharacterSize(20);
    text.setColor(sf::Color::Black);

    sf::Event event;

    // Création du socket de communication
    TcpSocket socket;

    if(socket.connectTo(argv[1], "9999"))
    {
        std::cout << "Connection established" << std::endl;
    }
    else
    {
        std::cout << "Connection failed" << std::endl;
        return EXIT_FAILURE;
    }

    // Lancement du thread de réception des balises
    std::thread th(receive, std::ref(balises_buffer), std::ref(socket));

    if (argc != 2) {
        std::cerr << "HOW TO USE " << argv[0]
                  << " server" << std::endl;
        return EXIT_FAILURE;
    }

    while(window.isOpen())
    {
        window.clear();

        window.draw(background);

        while(balises_buffer.canRead())
        {
            NewBalise* b = balises_buffer.pop();
            if(b->first == BaliseAction::New)
            {
                // On ajoute la balise aux balises connues
                if(doesBaliseExist(balises, b->second->getName()))
                {
                    delete balises[b->second->getName()];
                }
                balises[b->second->getName()] = b->second;
            }
            else
            {
                // On supprime la balise du tableau
                delete balises[b->second->getName()];
                balises.erase(b->second->getName());

                // On supprime la balise du NewBalise
                delete b->second;
            }

            // On supprime le NewBalise
            delete b;
        }

            system("clear");
        // Affichage des balises
        for(auto ba : balises)
        {
            Balise* b = ba.second;
            LOG(b->toString());
            boat.setPosition(b->getCoords().getX() + window.getSize().x/2, b->getCoords().getY() + window.getSize().y/2);
            text.setString(b->getName());
            text.setPosition(boat.getPosition().x, boat.getPosition().y + boat.getSize().y / 2);
            window.draw(boat);
            window.draw(text);
        }

        while(window.pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
                window.close();
        }

        //usleep(100 * 1000);

        window.display();
    }

    socket.closeSocket();

    return EXIT_SUCCESS;
}

void receive(Buffer<NewBalise *> &buffer, TcpSocket &socket)
{
    system("clear");

    Balise tmpBalise;
    NewBalise *b;

    while(true)
    {
        b = new NewBalise;

        if(socket.receiveObject<BaliseAction>(b->first))
        {
            socket.receiveObject<Balise>(tmpBalise);

            b->second = new Balise(tmpBalise);

            buffer.push(b);
        }
    }
}

bool doesBaliseExist(const std::map<std::string, Balise*> & balises, std::string name)
{
    auto it = balises.find(name);

    return it != balises.end();
}


