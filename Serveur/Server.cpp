#include "Server.h"

Server* Server::currentServer = nullptr;

Server::Server()
    :m_baliseListener{PORT_UDP}
{
    signal(SIGPIPE, signal_handler);
    signal(SIGKILL, signal_handler);
    signal(SIGINT, signal_handler);

    currentServer = this;

    // On écoute les clients TCP sur le port PORT_TCP
    m_clientListener.listenOn(PORT_TCP, 10);

    // On lance le serveur UDP
    m_threads.insert(new std::thread(thread_listenBalises, this));
    LOG("[SERVER] Start listening balises");

    // On lance le serveur TCP
    m_threads.insert(new std::thread(thread_acceptClients, this));
    LOG("[SERVER] Start accepting client");
}

Server::~Server()
{
    stop();
}

void Server::run()
{
    // Tableau des clients injoignables
    std::vector<TcpSocket*> failures;

    auto time = std::chrono::system_clock::now();

    while(m_isRunning)
    {
        time = std::chrono::system_clock::now();

        // On récupère les nouveaux clients
        while(m_clients_buffer.canRead())
        {
            TcpSocket* c = m_clients_buffer.pop();
            m_clients.insert(c);

            // On envoit toutes les balises connues
            sendAllBalisesDataTo(c);
        }

        // On récupère les nouvelles balises
        while(m_balises_buffer.canRead())
        {
            NewBalise* balise = m_balises_buffer.pop();

            // On prévient le client de la modification
            notifyClients(*balise, failures);

            if(balise->first == BaliseAction::New)
            {
                // On rajoute la balise aux balises connues
                insertBalise(balise->second);
            }
            else
            {
                // On efface cette balise qui n'existe plus
                m_balises.erase(balise->second->getName());

                // On supprime le pointeur de la balise, maintenant inutile
                delete balise->second;
            }

            // On supprime le NewBalise
            delete balise;
        }

        // On supprime les clients déconnectés
        disconnect(failures);

        // On réinitialise la liste des clients déconnectés
        failures.clear();
        failures.resize(0);

        m_treatment_time = std::chrono::system_clock::now() - time;

        display();
    }
}

void Server::display()
{
    unsigned int sleep_time = 100;

    usleep(sleep_time * 1000);

    system("clear");

    LOG("Balises :............." << m_balises.size());
    LOG("Connected clients :..." << m_clients.size());
    LOG("- - - - - - - - - - - - - - - -");
    LOG("Balise buffer :");
    LOG("   In queue :........." << m_balises_buffer.objectsInQueue());
    LOG("   Available space :.." << m_balises_buffer.availableSize());
    LOG("Client buffer :");
    LOG("   In queue :........." << m_clients_buffer.objectsInQueue());
    LOG("   Available space :.." << m_clients_buffer.availableSize());
    LOG("- - - - - - - - - - - - - - - ");
    LOG("Treatment time :......" << m_treatment_time.count() << " ms");
    LOG("Sleep time :.........." << sleep_time << " ms");
}

void Server::insertBalise(Balise* b)
{
    auto it = m_balises.find(b->getName());

    if(it != m_balises.end())
        delete m_balises[b->getName()];

    m_balises[b->getName()] = b;
}

void Server::notifyClients(NewBalise &balise, std::vector<TcpSocket *> & failures)
{
    for(TcpSocket * c : m_clients)
    {
        if(!sendBalisesDataTo(c, balise))
        {
            failures.push_back(c);
        }
    }
}

bool Server::sendBalisesDataTo(TcpSocket *dest, NewBalise & balise)
{
    bool success = true;

    // Envoit de l'action ( New / Delete )
    if(!dest->sendObject<BaliseAction>(balise.first))
    {
        success = false;
    }

    // Envoit de la balise
    if(!dest->sendObject<Balise>(*balise.second))
    {
        success = false;
    }

    return success;
}

bool Server::sendAllBalisesDataTo(TcpSocket *dest)
{
    bool success = true;

    for(auto & b : m_balises)
    {
        NewBalise balise;

        balise.first = BaliseAction::New;
        balise.second = b.second;

        if(!sendBalisesDataTo(dest, balise))
            success = false;
    }

    return success;
}

void Server::stop()
{
    if(m_isRunning == false)
        return;

    LOG("[SERVER] Stopping...");

    // Arrêt du serveur
    m_isRunning = false;

    // Déconnexion de tous les clients
    disconnectAll();

    // Fermeture du socket d'écoute
    m_clientListener.closeSocket();

    LOG("[SERVER] Stopped");
}

void Server::disconnectAll()
{
    for(TcpSocket* c : m_clients)
    {
        c->closeSocket();
    }
}

void Server::disconnect(TcpSocket *socket)
{
    auto it = m_clients.find(socket);

    if(it != m_clients.end())
    {
        (*it)->closeSocket();
        delete *it;

        m_clients.erase(socket);
    }
}

void Server::disconnect(std::vector<TcpSocket *> &sockets)
{
    for(TcpSocket* soc : sockets)
    {
        //LOG("[SERVER] Disconnecting client...");
        soc->closeSocket();
        delete soc;
    }

    for(TcpSocket* soc : sockets)
    {
        m_clients.erase(soc);
    }
}

void Server::thread_listenBalises(Server *obj)
{
    Balise buffer;

    while(obj->m_isRunning)
    {
        if(obj->m_baliseListener.receiveObject<Balise>(buffer))
        {          
            //LOG("[BALISE] A new balise has arrived ");
            //LOG("         " + buffer.toString());

            NewBalise *balise = new NewBalise;

            // On indique que l'on créé une nouvelle balise
            balise->first = BaliseAction::New;
            balise->second = new Balise(buffer);

            auto it = obj->m_balises.find(buffer.getName());

            // Si la balise existe déjà ...
            if(it != obj->m_balises.end())
            {
                // ... et quelle est en dehors de la map
                if(buffer.getCoords().getX() <= -obj->MAP_WIDTH || buffer.getCoords().getX() >= obj->MAP_WIDTH
                        || buffer.getCoords().getY() <= -obj->MAP_HEIGHT || buffer.getCoords().getY() >= obj->MAP_HEIGHT)
                {
                    // On indique que cette balise doit être supprimée
                    balise->first = BaliseAction::Delete;
                }
            }

            obj->m_balises_buffer.push(balise);
        }
    }
}

void Server::thread_acceptClients(Server *obj)
{
    TcpSocket* client;

    while(obj->m_isRunning)
    {
        // On accepte le nouveau client
        client = obj->m_clientListener.acceptConnection();

        obj->m_clients_buffer.push(client);
        //LOG("[SERVER] A new client is connected");
    }
}

void Server::signal_handler(int sig)
{
    switch(sig)
    {
    case SIGPIPE:
        LOG("[SERVER] Error on sending message");
        break;
    case SIGKILL:
        Server::getCurrentServer()->stop();
        break;
    case SIGINT:
        std::cout << "close" << std::endl;
        Server::getCurrentServer()->stop();
        break;
    }

}
