#ifndef SERVER_H
#define SERVER_H

#include <map>
#include <set>
#include <mutex>
#include <thread>
#include <csignal>
#include <chrono>

#include "../Common/TcpListener.h"
#include "../Common/TcpSocket.h"
#include "../Common/UdpSocket.h"
#include "../Common/Balise.h"
#include "../Common/Buffer.h"

#define LOG(x) std::cout << x << std::endl

// Serveur pour recevoir des balises en UDP
// et des clients en TCP

class Server
{
private:
    const unsigned int PORT_UDP = 9998;
    const unsigned int PORT_TCP = 9999;

    const int MAP_WIDTH = 800;
    const int MAP_HEIGHT = 450;

    static Server* currentServer;

    TcpListener m_clientListener;
    UdpSocket m_baliseListener;

    std::map<std::string, Balise*> m_balises;
    Buffer<NewBalise*> m_balises_buffer{1000};

    std::set<TcpSocket*> m_clients;
    Buffer<TcpSocket*> m_clients_buffer{1000};

    bool m_isRunning = true;

    std::set<std::thread*> m_threads;

    std::chrono::duration<double> m_treatment_time;

public:
    Server();
    ~Server();

    void run();

    static Server* getCurrentServer(){ return currentServer; }


private:

    // Envoit une balise et son action au socket 'dest'
    bool sendBalisesDataTo(TcpSocket *dest, NewBalise & balise);

    // Envoit toutes les balises au socket 'dest'
    bool sendAllBalisesDataTo(TcpSocket *dest);

    // Arrête le serveur
    void stop();

    // Déconnecte tous les clients
    void disconnectAll();

    // Déconnecte un socket
    void disconnect(TcpSocket* socket);

    // Déconnecte une liste de sockets
    void disconnect(std::vector<TcpSocket*> & sockets);

    // Ajoute une balise aux balises connues
    void insertBalise(Balise* b);

    void display();

    // Prévient les clients de l'arrivée / suppression d'une balise
    // Met les sockets injoignables dans failures
    void notifyClients(NewBalise & balise, std::vector<TcpSocket *> &failures);

    // Catch les signaux du système
    static void signal_handler(int sig);

    // Fonctions threadées

    // Serveur UDP
    static void thread_listenBalises(Server *obj);

    // Serveur TCP
    static void thread_acceptClients(Server *obj);

};

#endif // SERVER_H
