TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpthread

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra
SOURCES += main.cpp \
    ../Common/Balise.cpp \
    ../Common/make-sockaddr.cc \
    ../Common/request.cpp \
    ../Common/TcpListener.cpp \
    ../Common/TcpSocket.cpp \
    ../Common/UdpSocket.cpp \
    Server.cpp \
    ../Common/Buffer.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../Common/Balise.h \
    ../Common/make-sockaddr.h \
    ../Common/request.h \
    ../Common/TcpListener.h \
    ../Common/TcpSocket.h \
    ../Common/UdpSocket.h \
    Server.h \
    ../Common/Buffer.h

