TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra
SOURCES += main.cpp \
    ../Common/Balise.cpp \
    ../Common/make-sockaddr.cc \
    ../Common/UdpSocket.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../Common/Balise.h \
    ../Common/make-sockaddr.h \
    ../Common/UdpSocket.h

