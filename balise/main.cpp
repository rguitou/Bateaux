#include <iostream>
#include <unistd.h>
#include <cstdlib>

#include "../Common/make-sockaddr.h"
#include "../Common/Balise.h"

using namespace std;

int main(int argc, char* argv[])
{
    bool sendable;

    if (argc != 7)
    {
        std::cerr << "Wrong parameters: " << argv[0] << " <server> <boatname> <coordx> <coordy> <speedx> <speedy>" << std::endl;
        return EXIT_FAILURE;
    }

    auto serveradress = remote_socket_address(argv[1], SOCK_DGRAM, "9998");
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    Balise b(argv[2],atoi(argv[3]),atoi(argv[4]), atoi(argv[5]), atoi(argv[6])); // on cree la balise avec les parametres d'entrée

    int n = sendto(fd,&b, sizeof b, 0, (sockaddr *) &serveradress, sizeof serveradress);
    if (n == sizeof b)
    {
        sendable = true;
        std::cout << "Succesfully sent" << std::endl;
    } else {
        std::cout << "Error" << std::endl;
    }

    while(sendable)
    {
        sleep(1);
        std::cout << "1 second update attempt" << std::endl;
        b.updateCoords();
        if (b.CoordsinBounds())
        {
            int n = sendto(fd,&b, sizeof b, 0, (sockaddr *) &serveradress, sizeof serveradress);
            if (n == sizeof b)
            {
                std::cout << "Succesfully sent" << std::endl;
            } else {
                std::cout << "Error" << std::endl;
            }
        }
        else
        {
            sendable = false;
        }
    }
    close(fd);
}

